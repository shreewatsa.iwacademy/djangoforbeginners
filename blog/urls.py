from django.urls import path
from . import views

app_name = "blog"

urlpatterns = [
    path("post/", views.BlogListView.as_view(), name="blog_list_home"),
    path("post/<int:pk>/", views.BlogDetailView.as_view(), name="post_detail"),
    path("post/new/", views.BlogCreateView.as_view(), name="post_new"),
    path("post/<int:pk>/update/", views.BlogUpdateView.as_view(), name="post_update"),
    path("post/<int:pk>/delete/", views.BlogDeleteView.as_view(), name="post_delete"),

]
