from django.db import models
from django.shortcuts import reverse
from django.contrib.auth import get_user_model
User = get_user_model()

class Post(models.Model):
    author          = models.ForeignKey(User, on_delete=models.CASCADE)
    title           = models.CharField(max_length=100)
    body            = models.TextField()
    created_at      = models.DateTimeField(auto_now_add=True)
    updated_at      = models.DateTimeField(auto_now=True)
    
    class Meta:
        verbose_name = "Post"
        verbose_name_plural = "Posts"

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse("blog:post_detail", kwargs={"pk": self.pk})  #args=[str(self.id)] can be used instead of kwargs.


class Comment(models.Model):
    article             = models.ForeignKey(Post, related_name="comments", on_delete=models.CASCADE)
    author              = models.ForeignKey(get_user_model(), on_delete=models.CASCADE)
    comment             = models.CharField(max_length=150)
    created_at          = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.comment
    
    def get_absolute_url(self):
        return reverse("blog:post_detail", kwargs={"pk": self.article})
    
