
from django.urls import reverse_lazy
from django.shortcuts import render
from django.views.generic.edit import CreateView

from . import forms

class SignUpView(CreateView):
    form_class = forms.CustomUserCreationForm
    success_url = reverse_lazy("login")
    template_name = "registration/signup.html"

    