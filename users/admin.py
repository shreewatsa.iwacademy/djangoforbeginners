from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from . import forms, models

# Register your models here.

class CustomUserAdmin(UserAdmin):
    add_form        = forms.UserCreationForm
    form            = forms.UserChangeForm
    model           = models.CustomUser
    list_display    = ['username', 'email', 'age', 'is_staff']

admin.site.register(models.CustomUser, CustomUserAdmin)