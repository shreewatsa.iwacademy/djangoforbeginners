from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from django import forms

from . import models

class CustomUserCreationForm(UserCreationForm):
    class Meta(UserCreationForm.Meta):
        model = models.CustomUser
        # fields = UserCreationForm.Meta.fields + ("age",)
        fields = ["username", 'email', 'age']

class CustomUserChangeForm(UserChangeForm):
    class Meta:
        model = models.CustomUser
        # fields = UserChangeForm.Meta.fields
        fields = ["username", 'email', 'age']

        