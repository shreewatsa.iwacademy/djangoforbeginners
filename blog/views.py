from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.exceptions import PermissionDenied
from django.shortcuts import render
from django.views.generic import TemplateView, ListView, DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView, FormMixin
from django.urls import reverse_lazy, reverse
from django.http import HttpResponseForbidden


from . import models
from .forms import CommentForm

class BlogListView(ListView):
    model = models.Post
    template_name= "blog/blog_list_home.html"

class BlogDetailView(FormMixin, DetailView):
    model = models.Post
    context_object_name = "post"
    form_class = CommentForm

    def get_success_url(self):
        return reverse("blog:post_detail", kwargs={'pk':self.object.pk})
            
    def post(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return HttpResponseForbidden()
        self.object = self.get_object()
        form = self.get_form()
        form.article = self.object
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def form_valid(self, form):
        f = form.save(commit=False)
        f.author = self.request.user
        f.article = self.get_object()
        f.save()
        return super().form_valid(form)
    

class BlogCreateView(LoginRequiredMixin, CreateView):
    model = models.Post
    fields = ("title","body")
    template_name = "blog/post_form.html"
    login_url = "login"

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)

class BlogUpdateView(LoginRequiredMixin, UpdateView):
    model = models.Post
    fields = ["title",'body']
    template_name = "blog/post_form.html"

    def dispatch(self, request, *args, **kwargs):
        obj = self.get_object()
        if request.user != obj.author:
            raise PermissionDenied
        return super().dispatch(request, *args, **kwargs)

class BlogDeleteView(LoginRequiredMixin, DeleteView):
    model = models.Post
    template_name = "blog/post_delete.html"
    success_url = reverse_lazy("blog:blog_list_home") 

    def dispatch(self, request, *args, **kwargs):
        obj = self.get_object()
        if request.user != obj.author:
            raise PermissionDenied
        return super().dispatch(request, *args, **kwargs)