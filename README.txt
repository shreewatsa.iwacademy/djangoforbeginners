URL : https://newspaperdj.herokuapp.com/
https://git.heroku.com/newspaperdj.git


## Process in Heroku Deployment

$ heroku login
$ touch Procfile
Add this line to Procfile > web: gunicorn <project_name.wsgi> --log-file -
$ pip install gunicorn
$ heroku create <proj_name_for_url>
$ heroku git:remote -a <proj_name_for_url>
$ heroku config:set DISABLE_COLLECTSTATIC=1
$ git push heroku master
$ heroku ps:scale web=1
$ heroku open

# Note : heroku automatically configures us an instance of postgres database.
# Some other helpful heroku commands
$ heroku config


# django.contrib.admin.templates.registration.password_reset_email.html
